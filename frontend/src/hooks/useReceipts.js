import { useState } from 'react';

export const useReceipts = () => {
  const [receipts, setReceipts] = useState([
    { title: 'Hotel', price: 120.00, category: 'Accommodation' },
    { title: 'Taxi', price: 35.50, category: 'Transport' },
    { title: 'Dinner', price: 45.75, category: 'Meals' }
  ]);

  const addReceipt = (receipt) => {
    setReceipts(currentReceipts => [...currentReceipts, receipt]);
  };

  return {
    receipts,
    addReceipt
  };
};