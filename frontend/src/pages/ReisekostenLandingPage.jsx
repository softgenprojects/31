import React from 'react';
import { Box, Heading, Text, Container, SimpleGrid } from '@chakra-ui/react';
import { FaFacebook, FaTwitter, FaInstagram } from 'react-icons/fa';
import { NavigationBar } from '@components/NavigationBar';
import { Footer } from '@components/Footer';
import { BenefitsSection } from '@components/BenefitsSection';
import { ReceiptForm } from '@components/ReceiptForm';
import { ReceiptList } from '@components/ReceiptList';
import { useReceipts } from '@hooks/useReceipts';

export default function ReisekostenLandingPage() {
  const { receipts, addReceipt } = useReceipts();

  return (
    <Box>
      <NavigationBar />

      <Container as='main' maxW='container.xl' py={10}>
        <SimpleGrid columns={{ base: 1, md: 2 }} spacing={10}>
          <Box>
            <Heading as='h2' mb={4}>Reisekostenabrechnung f\u00fcr Unternehmen</Heading>
            <Text fontSize='lg' mb={4}>Efficiently manage your travel expenses and reimbursements with our comprehensive solution.</Text>
            <Text mb={4}>Our service offers:</Text>
            <ul>
              <li>Automated expense tracking</li>
              <li>Easy integration with accounting software</li>
              <li>Customizable approval workflows</li>
              <li>Real-time budget monitoring</li>
            </ul>
          </Box>
          <Box bgGradient='linear(to-r, teal.500, green.500)' height='300px' borderRadius='md'></Box>
        </SimpleGrid>
        <Box py={10}>
          <ReceiptForm addReceipt={addReceipt} />
          <ReceiptList receipts={receipts} />
          <BenefitsSection />
        </Box>
      </Container>

      <Footer />
    </Box>
  );
};