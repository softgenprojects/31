import { Table, Thead, Tbody, Tr, Th, Td } from '@chakra-ui/react';

export const ReceiptList = () => {
  const receipts = [
    { title: 'Taxi', price: '34.99', category: 'Transport' },
    { title: 'Lunch', price: '15.75', category: 'Food' },
    { title: 'Hotel', price: '120.00', category: 'Accommodation' }
  ];

  return (
    <Table variant='simple'>
      <Thead>
        <Tr>
          <Th>Title</Th>
          <Th isNumeric>Price</Th>
          <Th>Category</Th>
        </Tr>
      </Thead>
      <Tbody>
        {receipts.map((receipt, index) => (
          <Tr key={index}>
            <Td>{receipt.title}</Td>
            <Td isNumeric>{receipt.price}</Td>
            <Td>{receipt.category}</Td>
          </Tr>
        ))}
      </Tbody>
    </Table>
  );
};