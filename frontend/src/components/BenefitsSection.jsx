import { Box, Heading, List, ListItem } from '@chakra-ui/react';

export const BenefitsSection = () => {
  return (
    <Box p={[4, 6, 8]} borderWidth='1px' borderRadius='lg' overflow='hidden'>
      <Heading as='h3' size='lg' mb={4} textAlign='center'>
        Benefits of Reisekostenabrechnung
      </Heading>
      <List spacing={3}>
        <ListItem>Streamlined expense management</ListItem>
        <ListItem>Automated report generation</ListItem>
        <ListItem>Compliance with legal requirements</ListItem>
        <ListItem>Insights into spending patterns</ListItem>
      </List>
    </Box>
  );
};