import React from 'react';
import { FormControl, FormLabel, Input, Select, Button } from '@chakra-ui/react';

export const ReceiptForm = () => {
  const handleSubmit = (event) => {
    event.preventDefault();
    // Handle the form submission logic here
  };

  return (
    <form onSubmit={handleSubmit}>
      <FormControl isRequired>
        <FormLabel htmlFor='title'>Title</FormLabel>
        <Input id='title' placeholder='Enter receipt title' />
      </FormControl>

      <FormControl isRequired mt={4}>
        <FormLabel htmlFor='price'>Price</FormLabel>
        <Input id='price' placeholder='Enter price' type='number' />
      </FormControl>

      <FormControl isRequired mt={4}>
        <FormLabel htmlFor='category'>Category</FormLabel>
        <Select id='category' placeholder='Select category'>
          <option value='travel'>Travel</option>
          <option value='accommodation'>Accommodation</option>
          <option value='food'>Food</option>
          <option value='other'>Other</option>
        </Select>
      </FormControl>

      <Button mt={4} colorScheme='blue' type='submit'>Submit</Button>
    </form>
  );
};
