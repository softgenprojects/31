import { Flex, Box, Link } from '@chakra-ui/react';

export const NavigationBar = () => {
  return (
    <Flex
      as='nav'
      align='center'
      justify='space-between'
      wrap='wrap'
      padding='1.5rem'
      bg='blue.500'
      color='white'
      {...{ base: 'column', md: 'row' }}
    >
      <Flex align='center' mr={5}>
        <Box as='span' fontWeight='bold' fontSize='xl'>
          KRAEMER
        </Box>
      </Flex>

      <Box
        display={{ base: 'block', md: 'none' }}
        onClick={() => {}}
      >
        {/* Here would be a menu icon for mobile view */}
      </Box>

      <Box
        display={{ base: 'none', md: 'block' }}
        flexBasis={{ base: '100%', md: 'auto' }}
      >
        <Flex align='center' justify='flex-end' direction='row' pt={[4, 4, 0, 0]}>
          <Link href='#' px={2} py={1} rounded={'md'}>
            Home
          </Link>
          <Link href='#' px={2} py={1} rounded={'md'}>
            Features
          </Link>
          <Link href='#' px={2} py={1} rounded={'md'}>
            Pricing
          </Link>
          <Link href='#' px={2} py={1} rounded={'md'}>
            About
          </Link>
        </Flex>
      </Box>
    </Flex>
  );
};
