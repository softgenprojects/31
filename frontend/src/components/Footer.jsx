import { Flex, Box, Text, Icon } from '@chakra-ui/react';
import { FaTwitter, FaFacebookF, FaLinkedinIn } from 'react-icons/fa';

export const Footer = () => {
  return (
    <Flex
      as='footer'
      direction={{ base: 'column', md: 'row' }}
      align='center'
      justify='space-between'
      wrap='wrap'
      py='1rem'
      px={{ base: '1rem', md: '2rem' }}
      bg='gray.700'
      color='white'
      mt='auto'
    >
      <Box>
        <Text fontSize={{ base: 'sm', md: 'md' }} mb={{ base: '0.5rem', md: '0' }}>
          © 2023 Reisekostenabrechnung für Unternehmen
        </Text>
      </Box>
      <Box>
        <Flex gridGap='12px'>
          <Icon as={FaTwitter} w={6} h={6} />
          <Icon as={FaFacebookF} w={6} h={6} />
          <Icon as={FaLinkedinIn} w={6} h={6} />
        </Flex>
      </Box>
    </Flex>
  );
};
